# SPDX-FileCopyrightText: 2024 zocker <zocker@10zen.eu>
# SPDX-License-Identifier: GPL-3.0-or-later

PREFIX ?= "/usr/local"
DESTDIR ?= "$(PREFIX)/bin"

lecho: lecho.ha
	hare build -o $@ $<

check: lecho.ha
	hare build -lc -o $@ $<
	valgrind ./$@ lecho.ha 1
	valgrind ./$@ -f lecho.ha -l 1

.PHONY: clean
clean:
	rm -f lecho check

install: lecho
	strip --strip-all $<
	install -m755 $< $(DESTDIR)
